<?php

/**
 * @file
 * Contains the theme's settings form.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\File;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function kaziranga_form_system_theme_settings_alter(&$form, FormStateInterface &$form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  // No of banner is currently fixed to 3
  // Have to made it configurable in later release.
  $no_of_banner = 3;
  $form['kaziranga_slider'] = [
    '#type' => 'details',
    '#title' => t('Kaziranga Slider Settings'),
    '#open' => TRUE,
  ];
  for ($i = 1; $i <= $no_of_banner; $i++) {
    $form['kaziranga_slider']['kaziranga_banner_' . $i] = [
      '#type' => 'managed_file',
      '#title' => t('Banner ' . $i),
      '#upload_location' => 'public://images/',
      '#default_value' => theme_get_setting('kaziranga_banner_' . $i),
      '#description' => t("This Banners will show in Homepage Slider."),
    ];
  }
  $form['#submit'][] = 'kaziranga_form_system_theme_settings_submit';
}


function kaziranga_form_system_theme_settings_submit(&$form, $form_state) {
  for ($i = 1; $i <= 3; $i++) {
    $image_fid = $form_state->getValue('kaziranga_banner_' . $i);
    if (count($image_fid) > 0) {
      /* Fetch the array of the file stored temporarily in database */
      $image = $form_state->getValue('kaziranga_banner_' . $i);

      if (is_object($image)) {
        // Check to make sure that the file is set to be permanent.
        if (!$image->isPermanent()) {
          $image->setPermanent();
          $image->save();
          // Add a reference to prevent warnings.
          $file_usage = \Drupal::service('file.usage');
          $file_usage->add($image, 'kaziranga_banner', 'theme', 1);
        }
      }
    }
  }
}
