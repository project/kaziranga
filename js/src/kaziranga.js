(function ($) {
  'use strict';

  Drupal.behaviors.kaziranga = {
    attach: function(context, settings) {
      $('.flexslider').flexslider({
        animation: "slide",
        rtl: true,
        itemWidth: 0
      })
    }
  }
}(jQuery));